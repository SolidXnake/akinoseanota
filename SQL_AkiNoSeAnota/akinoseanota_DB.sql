﻿CREATE TABLE meetings(
id serial primary key not null,
place text not null,
meeting_date date not null,
meeting_hour time without time zone not null,
meeting_subject text not null
);

CREATE TABLE subjects(
id serial primary key not null,
description text not null,
duration time without time zone not null,
meeting_id int references meetings(id) not null
);

CREATE TABLE agreements(
id serial primary key not null,
detail text not null,
subject_id int references subjects(id) not null
);

CREATE TABLE tasks(
id serial primary key not null,
description text not null,
completion_date date not null,
status text,
subject_id int references subjects(id) not null
);

CREATE TABLE people(
id serial primary key not null,
name character varying(30) not null,
lastname character varying(30) not null,
email text not null,
phone text not null
);

CREATE TABLE task_people(
id serial primary key not null,
person_id int references people(id) not null,
task_id int references tasks(id) not null
);

CREATE TABLE meeting_people(
id serial primary key not null,
meeting_id int references meetings(id) not null,
person_id int references people(id) not null
);


