# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160515023027) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agreements", force: :cascade do |t|
    t.string   "detail"
    t.integer  "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "agreements", ["subject_id"], name: "index_agreements_on_subject_id", using: :btree

  create_table "meeting_people", force: :cascade do |t|
    t.integer  "meeting_id"
    t.integer  "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "meeting_people", ["meeting_id"], name: "index_meeting_people_on_meeting_id", using: :btree
  add_index "meeting_people", ["person_id"], name: "index_meeting_people_on_person_id", using: :btree

  create_table "meetings", force: :cascade do |t|
    t.string   "place"
    t.date     "meeting_date"
    t.time     "meeting_hour"
    t.string   "meeting_subject"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "name"
    t.string   "lastname"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "description"
    t.time     "duration"
    t.integer  "meeting_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "subjects", ["meeting_id"], name: "index_subjects_on_meeting_id", using: :btree

  create_table "task_people", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "task_people", ["person_id"], name: "index_task_people_on_person_id", using: :btree
  add_index "task_people", ["task_id"], name: "index_task_people_on_task_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.string   "description"
    t.date     "completion_date"
    t.string   "status"
    t.integer  "subject_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "tasks", ["subject_id"], name: "index_tasks_on_subject_id", using: :btree

  add_foreign_key "agreements", "subjects"
  add_foreign_key "meeting_people", "meetings"
  add_foreign_key "meeting_people", "people"
  add_foreign_key "subjects", "meetings"
  add_foreign_key "task_people", "people"
  add_foreign_key "task_people", "tasks"
  add_foreign_key "tasks", "subjects"
end
