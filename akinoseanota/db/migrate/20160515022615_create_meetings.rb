class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :place
      t.date :meeting_date
      t.time :meeting_hour
      t.string :meeting_subject

      t.timestamps null: false
    end
  end
end
