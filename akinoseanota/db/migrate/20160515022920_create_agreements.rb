class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.string :detail
      t.references :subject, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
