# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Meeting.create(place: "Academia Hack", meeting_date: '2016-05-16', meeting_hour: '09:00:00', meeting_subject: "Project's Day") # id: 1
Meeting.create(place: "Altamira", meeting_date: '2016-05-20', meeting_hour: '10:00:00', meeting_subject: "Revelion") # id: 2
Meeting.create(place: "La Guaira",meeting_date: '2016-05-20', meeting_hour: '15:00:00', meeting_subject: "Cleaning") # id: 3

Subject.create(description: "Reviewing", duration: '01:00:00', meeting_id: 1) # id: 1
Subject.create(description: "cerial's meeting", duration: '00:30:00', meeting_id: 1) # id: 2
Subject.create(description: "normal class", duration: '07:00:00', meeting_id: 1) # id: 3
Subject.create(description: "Discuss about political situation",duration:'01:00:00', meeting_id: 2) # id: 4
Subject.create(description: "Discuss about the enviroment",duration:'00:30:00', meeting_id: 3) # id: 5

Agreement.create(detail: "Everyone did good",subject_id: 1) # id: 1
Agreement.create(detail: "It should be repeated the cerial's day",subject_id: 2) # id: 2
Agreement.create(detail: "This situation stinks",subject_id: 4) # id: 3

Task.create(description: "Purchase more cereal", completion_date: '2016-05-17', status: "init", subject_id: 2) # id: 1
Task.create(description: "Purchase new cups",completion_date:'2016-05-17',status: "init", subject_id:2) # id: 2
Task.create(description: "Study more rails", completion_date: '2016-05-17',status:"init",subject_id: 3) # id: 3
Task.create(description: "Resume about the subject", completion_date: '2016-05-23',status:"init",subject_id:4) # id: 4
Task.create(description: "Clean all the beach", completion_date: '2016-06-27',status:"init",subject_id:5) # id: 5

Person.create(name:"Innocente", lastname: "Agostinelli", email:"innoagostinelli@mail.com",phone:"1111111111") # id: 1
Person.create(name:"Victor",lastname:"Padula",email:"vicpadula@mail.com",phone:"2222222222") # id: 2
Person.create(name:"Elieser",lastname:"Pereira",email:"elieserp@mail.com",phone:"3333333333") # id: 3
Person.create(name:"Wigrelys",lastname:"sanabria",email:"wisanabria@mail.com",phone:"4444444444") # id: 4
Person.create(name:"Jenny",lastname:"de la concepcion",email:"jenn@mail.com",phone:"5555555555") # id: 5
Person.create(name:"Pedro",lastname:"rojas",email:"peterojas@mail.com",phone:"666666666") # id: 6
Person.create(name:"Javier",lastname:"torrealba",email:"javtorre@mail.com",phone:"777777777") # id: 7


MeetingPerson.create(meeting_id:1,person_id:1) # id: 1
MeetingPerson.create(meeting_id:1,person_id:2) # id: 2
MeetingPerson.create(meeting_id:1,person_id:3) # id: 3
MeetingPerson.create(meeting_id:1,person_id:4) # id: 4
MeetingPerson.create(meeting_id:1,person_id:5) # id: 5
MeetingPerson.create(meeting_id:1,person_id:6) # id: 6
MeetingPerson.create(meeting_id:1,person_id:7) # id: 7
MeetingPerson.create(meeting_id:2,person_id:1) # id: 8
MeetingPerson.create(meeting_id:2,person_id:2) # id: 9
MeetingPerson.create(meeting_id:2,person_id:3) # id: 10
MeetingPerson.create(meeting_id:3,person_id:4) # id: 11
MeetingPerson.create(meeting_id:3,person_id:5) # id: 12
MeetingPerson.create(meeting_id:3,person_id:6) # id: 13
MeetingPerson.create(meeting_id:3,person_id:7) # id: 14

TaskPerson.create(task_id:1,person_id:1) # id: 1
TaskPerson.create(task_id:1,person_id:2) # id: 2
TaskPerson.create(task_id:2,person_id:3) # id: 3
TaskPerson.create(task_id:2,person_id:4) # id: 4
TaskPerson.create(task_id:3,person_id:5) # id: 5
TaskPerson.create(task_id:3,person_id:6) # id: 6
TaskPerson.create(task_id:3,person_id:7) # id: 7
TaskPerson.create(task_id:4,person_id:2) # id: 8
TaskPerson.create(task_id:5,person_id:4) # id: 9

TaskPerson.create(task_id:5,person_id:5)
TaskPerson.create(task_id:5,person_id:6)
TaskPerson.create(task_id:5,person_id:7)















