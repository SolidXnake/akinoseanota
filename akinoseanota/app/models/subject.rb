class Subject < ActiveRecord::Base
  belongs_to :meeting
  has_many :agreements, dependent: :destroy
  has_many :tasks, dependent: :destroy

	validates :description, presence:true
	validates :duration, presence:true
	validates_associated :meeting
end
