class Meeting < ActiveRecord::Base
	
	has_many :subjects, dependent: :destroy

	has_many :people, through: :meeting_people
	has_many :meeting_people, dependent: :destroy

	validates :place, presence:true
	validates :meeting_date, presence:true
	validates :meeting_hour, presence:true
	validates :meeting_subject, presence:true


	#Method that shows all the tasks in the respective meeting
	def tasks()
		meeting_tasks = []
		subjects.includes(:tasks).each do |sub|
			meeting_tasks.push(sub.tasks)
		end
		meeting_tasks.flatten.each{|task| puts task.description}
	end

	#Method that shows the tasks with a specific status
	def tasks(stat)
		meeting_tasks = []
		subjects.includes(:tasks).each do |sub|
			meeting_tasks.push(sub.tasks)
		end
		meeting_tasks.flatten.each{|task| puts task.description if task.status == stat}
	end

	#Method that shows all the agreements in the respective meeting

	def agreements()
		meeting_agreements = []
		subjects.includes(:agreements).each do |sub|
			meeting_agreements.push(sub.agreements)
		end
		meeting_tasks.flatten.each{|agreement| puts agreement.detail}
	end
end
