class Task < ActiveRecord::Base
  belongs_to :subject

  has_many :people, through: :task_people
  has_many :task_people, dependent: :destroy


  validates_associated :subject
  validates_associated :people

  validates :description, presence:true
  validates :completion_date, presence:true
  validates :status, presence:true
end
