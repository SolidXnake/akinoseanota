class Agreement < ActiveRecord::Base
  belongs_to :subject

  validates :detail, presence:true
  validates_associated :subject
end
