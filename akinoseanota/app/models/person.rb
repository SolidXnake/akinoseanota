class Person < ActiveRecord::Base
	has_many :meetings, through: :meeting_people
	has_many :meeting_people , dependent: :destroy	

	has_many :tasks, through: :task_people
	has_many :task_people, dependent: :destroy

	validates :name, presence:true
	validates :lastname, presence:true
	validates :phone, presence:true, numericality: {only_integer: true}

	protected

	before_validation :set_email

	before_create do 
		self.name = self.name.capitalize
		self.lastname = self.lastname.capitalize
	end

	def set_email
		self.email = "#{name}+#{lastname}@mail.com" unless self.email.blank?
	end
end
